package com.tb.pizza_palace.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
public class ErrorResponse {


    @AllArgsConstructor
    @NoArgsConstructor
    public static class ErrorMsg<T>{
        public int code;
        public T message;

    }

    private boolean success;
    private String message;
    private ErrorMsg<?> error;

}