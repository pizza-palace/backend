package com.tb.pizza_palace.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class Customer {

    private Integer customerId;

    @NotEmpty(message = "firstName can't be null")
    @Size(min = 3, max = 30, message = "Char size must be between 3 - 30 chars")
    private String firstName;


    private String lastName;


    private String address;


    private String phoneNumber;

    @Email(message = "Provide valid email address")
    @NotNull(message = "Email can't be null")
    private String emailAddress;

    @NotNull(message = "Password cannot be null")
    private String password;

}
