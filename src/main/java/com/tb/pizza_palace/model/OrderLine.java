package com.tb.pizza_palace.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderLine {
    private Integer orderLineId;
    private Integer orderId;
    private Integer pizzaId;
    private String pizzaSize;
    private Integer quantity;
    private Integer totalPrice;
    private String crust;
    private String toppings;
}
