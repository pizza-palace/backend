package com.tb.pizza_palace.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Setter
@Getter
public class Pizza {

    private Integer pizzaId;

    @NotEmpty(message = "pizza name can't be empty")
    private String name;

    @Size(min = 10, max = 255, message = "description characters must be between 10 to 255")
    private String description;
    private String pizzaType;
    private String imageURL;

    @NotNull(message = "Regular pizza price must be provided")
    @PositiveOrZero(message = "Price must be positive")
    private Integer priceRegular; //priceRegular

    @NotNull(message = "Medium pizza price must be provided")
    @PositiveOrZero(message = "Price must be positive")
    private Integer priceMedium;

    @NotNull(message = "Regular pizza price must be provided")
    @PositiveOrZero(message = "Price must be positive")
    private Integer priceLarge;
}
