package com.tb.pizza_palace.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Admin {
    private Integer id;
    private String name;
    private String emailAddress;
    private String password;
}
