package com.tb.pizza_palace.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;


@Getter
@Setter
public class Order {
    private Integer orderId;

    @NotNull(message = "Customer ID can't be null")
    private Integer customerId;


    private String status;

    @NotNull(message = "Total amount can't be null")
    private Integer totalAmount;

    @NotNull(message = "Date time can't be null")
    private Date orderDateTime;

    @NotNull(message = "Address can't be null")
    @Size(min = 10, max = 255, message = "Characters for address should be in range of 10 to 255")
    private String deliveryAddress;
}