package com.tb.pizza_palace.controller;

import com.tb.pizza_palace.model.Pizza;
import com.tb.pizza_palace.service.PizzaMapper;
import com.tb.pizza_palace.utils.ErrorResponse;
import com.tb.pizza_palace.utils.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/pizza")
public class PizzaController {
    @Autowired
    PizzaMapper pizzaMapper;

    @GetMapping
    public ResponseEntity<?> selectAllPizzas(){
        List<Pizza> list = Collections.emptyList();
        try{
            list = pizzaMapper.selectAllPizzas();
        }catch (Exception e){
            ErrorResponse errorResponse = new ErrorResponse(false, "Failed to fetch pizza data", new ErrorResponse.ErrorMsg<>(404, "Failed to fetch pizza data"));
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }

        SuccessResponse<List<Pizza>> customerSuccessResponse = new SuccessResponse<>(true, "Successfully fetched pizza details", list);
        return ResponseEntity.status(HttpStatus.OK).body(customerSuccessResponse);
    }

    @GetMapping("/{pizzaId}")
    public ResponseEntity<?> selectPizza(@PathVariable Integer pizzaId){
        Pizza pizza = pizzaMapper.selectPizza(pizzaId);

        if(pizza != null){
            SuccessResponse<Pizza> successResponse = new SuccessResponse<>(true, "Data Found", pizza);
            return ResponseEntity.status(HttpStatus.OK).body(successResponse);
        }
        ErrorResponse errorResponse = new ErrorResponse(false, "Failed to fetch pizza data", new ErrorResponse.ErrorMsg<>(404, "Invalid pizza ID."));
        return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @PostMapping
    public ResponseEntity<?> insertPizza(@Valid @RequestBody Pizza pizza){

        pizzaMapper.insertPizza(pizza);
        SuccessResponse<Pizza> successResponse = new SuccessResponse<>(true, "Successfully created pizza", pizza);
        return ResponseEntity.status(HttpStatus.OK).body(successResponse);
    }

    @PutMapping("/{pizzaId}")
    public ResponseEntity<?> updatePizza(@PathVariable Integer pizzaId,@RequestBody Pizza updateData){
        Pizza pizza = pizzaMapper.selectPizza(pizzaId);

        if(updateData.getName() != null) pizza.setName(updateData.getName());
        if(updateData.getDescription() != null) pizza.setDescription(updateData.getDescription());
        if(updateData.getImageURL() != null) pizza.setImageURL(updateData.getImageURL());
        if(updateData.getPizzaType() != null) pizza.setPizzaType(updateData.getPizzaType());
        if(updateData.getPriceRegular() != null) pizza.setPriceRegular(updateData.getPriceRegular());
        if(updateData.getPriceMedium() != null) pizza.setPriceMedium(updateData.getPriceMedium());
        if(updateData.getPriceLarge() != null) pizza.setPriceLarge(updateData.getPriceLarge());
        pizzaMapper.updatePizza(pizza);
        SuccessResponse<Pizza> successResponse = new SuccessResponse<>(true, "Successfully updated pizza details", pizza);
        return ResponseEntity.status(HttpStatus.OK).body(successResponse);
    }

    @DeleteMapping("/{pizzaId}")
    public ResponseEntity<?> deletePizza(@PathVariable Integer pizzaId){
        Pizza pizza = pizzaMapper.selectPizza(pizzaId);
        if(pizza != null){
            pizzaMapper.deletePizza(pizzaId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        ErrorResponse errorResponse = new ErrorResponse(false, "Failed to delete pizza", new ErrorResponse.ErrorMsg<>(404, "Failed to fetch pizza data"));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        List<String> errors = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String errorMessage = error.getDefaultMessage();
            errors.add(errorMessage);
        });
        return new ErrorResponse(false, "Failed to create customer record", new ErrorResponse.ErrorMsg<>(404, errors));
    }
}
