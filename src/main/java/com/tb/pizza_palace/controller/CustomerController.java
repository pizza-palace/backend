package com.tb.pizza_palace.controller;


import com.tb.pizza_palace.model.Customer;
import com.tb.pizza_palace.service.CustomerMapper;
import com.tb.pizza_palace.utils.ErrorResponse;
import com.tb.pizza_palace.utils.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    @Autowired
    CustomerMapper customerMapper;


    @PostMapping
    public ResponseEntity<?> insertCustomer(@Valid @RequestBody Customer customer){
       try {
           customerMapper.insertCustomer(customer);
           SuccessResponse<Customer> response = new SuccessResponse<>(true, "Successfully created customer", customer);
           return ResponseEntity.status(HttpStatus.OK).body(response);
       } catch (Exception e){
           String msg;
           if(e.getMessage().contains("ORA-00001")){
               msg = "Email already exist";
           } else {
               msg = "Table not exist";
           }
           ErrorResponse errorResponse = new ErrorResponse(false, "failed to register", new ErrorResponse.ErrorMsg<>(400, msg));
           return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
       }
    }



    @GetMapping
    public ResponseEntity<?> selectAllCustomers(){
        List<Customer> list = Collections.emptyList();

        try{
             list = customerMapper.selectAllCustomers();
        }catch (Exception e){
            ErrorResponse errorResponse = new ErrorResponse(false, "Failed to fetch customer data", new ErrorResponse.ErrorMsg<>(404, "Failed to fetch customer data"));
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }

        SuccessResponse<List<Customer>> customerSuccessResponse = new SuccessResponse<>(true, "Successfully fetched all customers data", list);
        return ResponseEntity.status(HttpStatus.OK).body(customerSuccessResponse);
    }

    @GetMapping("/email/{emailAddress}")
    public ResponseEntity<?> selectCustomer(@PathVariable String emailAddress){
        Customer customer = customerMapper.getUserByEmail(emailAddress);
        if(customer != null){
            SuccessResponse<Customer> customerSuccessResponse = new SuccessResponse<>(true, "Data Found", customer);
            return ResponseEntity.status(HttpStatus.OK).body(customerSuccessResponse);
        }
        ErrorResponse errorResponse = new ErrorResponse(false, "Failed to fetch customer data", new ErrorResponse.ErrorMsg<>(404,"Invalid email address."));
        return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @PutMapping("/{customerId}")
    public ResponseEntity<?> updateCustomer(@PathVariable Integer customerId, @RequestBody Customer updateCustomerData){
        Customer prevCustomerData = customerMapper.selectCustomer(customerId);
        if(prevCustomerData == null){
            ErrorResponse errorResponse = new ErrorResponse(false, "Customer data not found", new ErrorResponse.ErrorMsg<>(400,"Invalid customer ID."));
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        prevCustomerData.setCustomerId(prevCustomerData.getCustomerId());
        if(updateCustomerData.getFirstName() != null) prevCustomerData.setFirstName(updateCustomerData.getFirstName());
        if(updateCustomerData.getLastName() != null) prevCustomerData.setLastName(updateCustomerData.getLastName());
        if(updateCustomerData.getAddress() != null) prevCustomerData.setAddress(updateCustomerData.getAddress());
        if(updateCustomerData.getPhoneNumber() != null) prevCustomerData.setPhoneNumber(updateCustomerData.getPhoneNumber());
        if(updateCustomerData.getEmailAddress() != null) prevCustomerData.setEmailAddress(updateCustomerData.getEmailAddress());
        customerMapper.updateCustomer(prevCustomerData);
        SuccessResponse<Customer> successResponse = new SuccessResponse<>(true, "Successfully updated customer data", prevCustomerData);
        return ResponseEntity.status(HttpStatus.OK).body(successResponse);
    }



    @DeleteMapping("/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Integer customerId){
        Customer customer = customerMapper.selectCustomer(customerId);
        if(customer != null){
            customerMapper.deleteCustomer(customerId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        ErrorResponse errorResponse = new ErrorResponse(false, "Failed to delete customer", new ErrorResponse.ErrorMsg<>(404,"Invalid customer ID."));
        return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        List<String> errors = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String errorMessage = error.getDefaultMessage();
            errors.add(errorMessage);
        });
        return new ErrorResponse(false, "Failed to create customer record", new ErrorResponse.ErrorMsg<>(404, errors));
    }
}
