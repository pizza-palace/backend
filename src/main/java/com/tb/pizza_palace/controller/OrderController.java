package com.tb.pizza_palace.controller;

import com.tb.pizza_palace.model.Order;
import com.tb.pizza_palace.service.OrderMapper;
import com.tb.pizza_palace.utils.ErrorResponse;
import com.tb.pizza_palace.utils.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/api/v1/order")
public class OrderController {

    @Autowired
    OrderMapper orderMapper;


    @PostMapping
    public ResponseEntity<?> insertOrder(@Valid @RequestBody Order order){
        try{
            orderMapper.insertOrder(order);
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "Order created successfully", order));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to create order", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }

    @GetMapping
    public ResponseEntity<?> selectAllOrder(){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "All order details fetched", orderMapper.selectAllOrders()));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to fetch the data", new ErrorResponse.ErrorMsg<>(404, "Table or view not found")));
        }
    }

    @GetMapping("/orders-by-customer/{customerId}")
    public ResponseEntity<?> selectOrdersByCustomerId(@PathVariable Integer customerId){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "All order details fetched", orderMapper.selectOrdersByCustomerId(customerId)));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to fetch the data", new ErrorResponse.ErrorMsg<>(404, "Table or view not found")));
        }
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<?> selectOrder(@PathVariable Integer orderId){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "Fetched data for given id", orderMapper.selectOrder(orderId)));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to fetch the data", new ErrorResponse.ErrorMsg<>(404, "Table or view not found")));
        }
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<?> deleteOrder(@PathVariable Integer orderId){
        try{
            Order order = orderMapper.selectOrder(orderId);
            if(order != null){
                orderMapper.deleteOrder(orderId);
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Data not found.", new ErrorResponse.ErrorMsg<>(404, "Order data not found with given id")));
            }
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to delete data", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }

    @PutMapping("/{orderId}")
    public ResponseEntity<?> updateOrder(@PathVariable Integer orderId, @RequestBody Order body){
            Order order = orderMapper.selectOrder(orderId);
            if(body.getStatus() != null) order.setStatus(body.getStatus());
            if(body.getTotalAmount() != null) order.setTotalAmount(body.getTotalAmount());
            if(body.getDeliveryAddress() != null) order.setDeliveryAddress(body.getDeliveryAddress());

            try{
                orderMapper.updateOrder(order);
                return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "Order Data updated successfully", order));
            } catch (Exception e){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to update", new ErrorResponse.ErrorMsg<>(404, "Unable to update order")));
            }

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        List<String> errors = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String errorMessage = error.getDefaultMessage();
            errors.add(errorMessage);
        });
        return new ErrorResponse(false, "Failed to create customer record", new ErrorResponse.ErrorMsg<>(404, errors));
    }
}
