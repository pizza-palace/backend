package com.tb.pizza_palace.controller;


import com.tb.pizza_palace.model.Admin;
import com.tb.pizza_palace.service.AdminMapper;
import com.tb.pizza_palace.utils.ErrorResponse;
import com.tb.pizza_palace.utils.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/admin")
@CrossOrigin
public class AdminController {

    @Autowired
    AdminMapper adminMapper;

    @GetMapping("/{emailAddress}")
    public ResponseEntity<?> selectAdmin(@PathVariable String emailAddress){
        try {
            Admin admin = adminMapper.selectAdmin(emailAddress);
            if(admin != null)
                return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "Admin found", admin));
            else
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Admin not found", new ErrorResponse.ErrorMsg<>(404, "admin not found")));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Admin not found", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }
}
