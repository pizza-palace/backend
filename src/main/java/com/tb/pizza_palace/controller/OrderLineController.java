package com.tb.pizza_palace.controller;


import com.tb.pizza_palace.model.OrderLine;
import com.tb.pizza_palace.service.OrderLineMapper;
import com.tb.pizza_palace.utils.ErrorResponse;
import com.tb.pizza_palace.utils.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1/order-line")
public class OrderLineController {

    @Autowired
    OrderLineMapper orderLineMapper;

    @PostMapping
    public ResponseEntity<?> insertOrderLine(@RequestBody OrderLine orderLine){
        try {
            orderLineMapper.insertOrderLine(orderLine);
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "order line created successfully", orderLine));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to create order line", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }

    @GetMapping
    public ResponseEntity<?> selectAllOrderLine(){
        try{
            List<OrderLine> orderLineList =  orderLineMapper.selectAllOrderLine();
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "successfully fetched all order lines", orderLineList));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "failed to fetch data", new ErrorResponse.ErrorMsg<>(404, "failed to fetch data")));
        }
    }

    @GetMapping("/{orderLineId}")
    public ResponseEntity<?> selectOrderLine(@PathVariable Integer orderLineId){
        try{
            OrderLine orderLine =  orderLineMapper.selectOrderLine(orderLineId);
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "successfully fetched order line", orderLine));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "failed to fetch data", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }

    @GetMapping("/order-line-by-order/{orderId}")
    public ResponseEntity<?> selectOrderLineByOrderId(@PathVariable Integer orderId){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "successfully fetched order line", orderLineMapper.selectOrderLineByOrderId(orderId)));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "failed to fetch data", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<?> deleteOrderLineByOrderId(@PathVariable Integer orderId){
        try {
            orderLineMapper.deleteOrderLineByOrderId(orderId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to delete order line", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }

    @DeleteMapping("/by-order-line-id/{orderLineId}")
    public ResponseEntity<?> deleteOrderLine(@PathVariable Integer orderLineId){
        try {
            orderLineMapper.deleteOrderLine(orderLineId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to delete order line", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }

    @PutMapping("/{orderLineId}")
    public ResponseEntity<?> updateOrderLine(@PathVariable Integer orderLineId, @RequestBody OrderLine orderLine){


        OrderLine prevOrderLine = orderLineMapper.selectOrderLine(orderLineId);
        if(orderLine.getQuantity() != null) prevOrderLine.setQuantity(orderLine.getQuantity());
        if(orderLine.getPizzaSize() != null) prevOrderLine.setPizzaSize(orderLine.getPizzaSize());
        if(orderLine.getTotalPrice() != null) prevOrderLine.setTotalPrice(orderLine.getTotalPrice());
        if(orderLine.getCrust() != null) prevOrderLine.setCrust(orderLine.getCrust());
        if(orderLine.getToppings() != null) prevOrderLine.setToppings(orderLine.getToppings());

        try {
            orderLineMapper.updateOrderLine(orderLineId, prevOrderLine);
            return ResponseEntity.status(HttpStatus.OK).body(new SuccessResponse<>(true, "Order Line Data updated successfully", prevOrderLine));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(false, "Failed to update", new ErrorResponse.ErrorMsg<>(404, e.getMessage())));
        }
    }

}
