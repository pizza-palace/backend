package com.tb.pizza_palace.service;


import com.tb.pizza_palace.model.Pizza;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PizzaMapper {
    void insertPizza(Pizza pizza);
    List<Pizza> selectAllPizzas();
    Pizza selectPizza(Integer pizzaId);
    void deletePizza(Integer pizzaId);
    void updatePizza(Pizza pizza);
}
