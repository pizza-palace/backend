package com.tb.pizza_palace.service;

import com.tb.pizza_palace.model.OrderLine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderLineMapper {
    void insertOrderLine(OrderLine orderLine);
    List<OrderLine> selectAllOrderLine();
    OrderLine selectOrderLine(Integer orderLineId);
    void deleteOrderLineByOrderId(Integer orderId);
    void updateOrderLine(Integer orderLineId,@Param("orderLineObj") OrderLine orderLine);
    List<OrderLine> selectOrderLineByOrderId(Integer orderId);
    void deleteOrderLine(Integer orderLineId);
}
