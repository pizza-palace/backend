package com.tb.pizza_palace.service;


import com.tb.pizza_palace.model.Customer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CustomerMapper {
    void insertCustomer(Customer customer);
    List<Customer> selectAllCustomers();
    Customer selectCustomer(Integer customerId);
    void updateCustomer(Customer customer);
    void deleteCustomer(Integer customerId);
    Customer getUserByEmail(String emailAddress);
}