package com.tb.pizza_palace.service;

import com.tb.pizza_palace.model.Order;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderMapper {
    void insertOrder(Order order);
    List<Order> selectAllOrders();
    Order selectOrder(Integer orderId);
    void deleteOrder(Integer orderId);
    void updateOrder(Order order);
    List<Order> selectOrdersByCustomerId(Integer customerId);
}
