package com.tb.pizza_palace.service;

import com.tb.pizza_palace.model.Admin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper {
    Admin selectAdmin(String emailAddress);
}
